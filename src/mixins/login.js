
import * as apis from '../utils/apis.js';
import store from '../store';
import { mapMutations } from '@wepy/x';
export default {
  store,
  data: {
    sessionKey:'',
    encryptedData:'',
    iv:'',
    openid:'',
    phonenum:'',
    token:'',
    tokenKey:'',
    count:10,
    skip:0
  },
  methods: {
    ...mapMutations['changeNotice'],
    toast(title,icon='none'){
      wx.showToast({
        title,
        icon,
        duration: 2000
      })
    },
   
    getData () {
      const that =this
      wx.login({
        success(res) {
          if(res.code) {
            console.log(res)
            apis.wxlogin({code:res.code}).then(res=>{
              console.log(res,'dadadsa')
              const {result:{openid,session_key},returnCode} = res
              if(returnCode=='AAAAAAA'){
                  that.openid=openid
                  that.sessionKey=session_key
              }
            })
          } 
        },
        fail(res){
        }
      })
    },
    notice(){
      const that = this
      //  wx.showLoading({
      //   title: '加载中',
      // })
      apis.getnotices({
        token:that.token,
        tokenKey:that.tokenKey,
        count:10,
        skip:0

      }).then(res=>{
           if(res.returnCode==="AAAAAAA"){
             if(res.result &&res.result.unreadCount==0){
              that.$store.commit('changeNotice','')
             }else{
              that.$store.commit('changeNotice',res.result.unreadCount)
             }
            
            // wx.hideLoading()
           }
      }).catch(err=>{
            //  wx.hideLoading()
          })

    },
    getPhoneNumber(e) {
      if(e.$wx.detail.errMsg == 'getPhoneNumber:fail user deny'){
        wx.showToast({
          title: '您已取消授权',
          icon:'none',
          duration:5000
        })}else{
          const {$wx:{detail:{encryptedData,iv}}} = e
          let that = this
          that.encryptedData=encryptedData
          that.iv = iv
          that.getPhone()
        }
 
    },
    pushOpenid(){
      const that = this
       wx.showLoading({
        title: '加载中',
      })
      apis.login({
        loginname:that.phonenum,
        openid:that.openid,
        loginType:3
      }).then(res=>{
           if(res.returnCode==="AAAAAAA"){
            wx.setStorageSync('token',res.token)
            wx.setStorageSync('tokenKey',res.tokenKey)
            wx.setStorageSync('username',res.username)
            wx.setStorageSync('roleid',res.roleid)
            wx.showToast({
              title: "登录成功",
              icon: 'loading',
              duration: 1000
            });
            setTimeout(()=>{
              wx.switchTab({
                url: '/pages/postlist'
               })
            },1500 *0.9)
           }
      }).catch(err=>{
             wx.hideLoading()
          })

    },

    getPhone(){
      const that = this
       wx.showLoading({
        title: '加载中',
      })
      wx.checkSession({
        success: function() {
          apis.getphone({
            session_key:that.sessionKey,
            encryptedData:that.encryptedData,
            iv:that.iv,
          }).then(res=>{
             wx.hideLoading()
            if(res.returnCode=='AAAAAAA'){
            that.phonenum=res.result.phoneNumber
            // 获取当前页面路径
              that.pushOpenid()
            }
          }).catch(err=>{
             wx.hideLoading()
          })

        },
        fail: function () {
          
        }
      })
    },

  },
  created () {
    this.getData()
  }
}
