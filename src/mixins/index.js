
import * as apis from '../utils/apis.js';

 

export default {
  data: {
    sessionKey:'',
    encryptedData:'',
    iv:'',
    openid:'',
    phonenum:'',
  },
  methods: {
   
    toast(title,icon='none'){
      wx.showToast({
        title,
        icon,
        duration: 2000
      })
    },
    getData () {
      const that =this
      wx.login({
        success(res) {
          if(res.code) {
            console.log(res)
            apis.wxlogin({code:res.code}).then(res=>{
              console.log(res,'dadadsa')
              const {result:{openid,session_key},returnCode} = res
              if(returnCode=='AAAAAAA'){  
                  
                  that.openid=openid
                  that.sessionKey=session_key
              }
            })
          } else {
            console.log('登录失败！' + res.errMsg)
          }
        }
      })
    },
  
    getPhoneNumber(e) {
      console.log(e.$wx,'获取信息');
      const {$wx:{detail:{encryptedData,iv}}} = e
      let that = this
      that.encryptedData=encryptedData
      that.iv = iv
      that.getPhone()
    },

    getPhone(){
      const that = this
       wx.showLoading({
        title: '加载中',
      })
      wx.checkSession({
        success: function() {
          apis.getphone({
            session_key:that.sessionKey,
            encryptedData:that.encryptedData,
            iv:that.iv,
          }).then(res=>{
             wx.hideLoading()
            console.log(res,'dadadsa')
            const {result:{phoneNumber},returnCode} = res
            if(returnCode=='AAAAAAA'){
              that.phonenum = phoneNumber
            }
          }).catch(err=>{
             wx.hideLoading()
          })

        },
        fail: function () {
          // $Toast({content: '获取失败请重试'});
          //  that.login()
        }
      })
    },

  },
  created () {
    
  }
}
