import moment from 'moment'


const time = (o)=>{

    var n=new Date().getTime();
    var f=n-o;
    var bs=(f>=0?'前':'后');//判断时间点是在当前时间的 之前 还是 之后
    f=Math.abs(f);
    if(f<6e4){return '刚刚'}//小于60秒,刚刚
    if(f<36e5){return parseInt(f/6e4)+'分钟'+bs}//小于1小时,按分钟
    if(f<864e5){return parseInt(f/36e5)+'小时'+bs}//小于1天按小时
    if(f<2592e6){return moment(o).format('YYYY-MM-DD')}//小于1个月(30天),按天数
    if(f<31536e6){return moment(o).format('YYYY-MM-DD')}//小于1年(365天),按月数
    return moment(o).format('YYYY-MM-DD');//大于365天,按年算
}

export default time
