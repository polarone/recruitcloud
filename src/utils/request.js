import wepy from '@wepy/core'
const request = ( url,data = {},code,method)=>{
  return new Promise((resolve, reject) => {
    method = method ? "get":"post"
    wx.request({
      url:url,
      method: method,
      data: {
        head: {
          "transcode": code,
          "type": "h"
        },data
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        const {data:{returnCode,returnMsg}} =res
        console.log(res,'数据')
        if (returnCode != 'AAAAAAA') {
          wx.showToast({
            title: returnMsg,
            icon: 'none',
            duration: 2000
          })
          if(returnCode=='0000005'){
            wx.reLaunch({
              url: '/pages/logins'
            })
          }
        }
        resolve(res.data);
      },
      fail: function (err) {
        reject(err)
      }
    })
  })

}
export default request
