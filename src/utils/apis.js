import request  from './request'
  
// const url = 'https://nk.luwenkeji.com.cn'
// const url1 = 'https://tianqiapi.com/api'
const host = 'http://192.168.1.242:8080' // 本地环境
// const host = 'http://192.168.1.251:66' // 测试环境
// const host = 'https://yun.51jrq.com'  // 线上环境
const url = host+'/51hr/api'

const baseurl = url
// 获取二维码
const getcode = (params) => request(url+'/web/createwxaqrcode',params,'L0113',"get")
// 面试反馈列表查询
const getInterview = (params) => request(url+'/web/intefb',params,'L0107')
// 面试反馈列表查询
const remind = (params) => request(url+'/web/sendfbnotice',params,'L0112')
// 面试信息
const checkInfo = (params) => request(url+'/web/employeeinfo/getResumeEvaluationById',params,'L0066')

// // 七日天气
// const qrtq = (params) => request(url1,params,'L0001')
// 更新已经信息通知
const notice = (params) => request(url+'/web/updnotice',params,'L0111')

// 登录
const login = (params) => request(url+'/web/login',params,'L0001')

// 发送验证码
const vcode= (params) => request(url+'/web/vcode',params,'L0101')

// 修改密码
const changepwd = (params) => request(url+'/web/changepwd',params,'L0002')

// 微信登录
const wxlogin = (params) => request(url+'/web/code2Session',params,'L0102')

// 获取手机号
const getphone = (params) => request(url+'/web/getWXPhoneNumber',params,'L0103')

// 退出登录
const loginout = (params) => request(url+'/web/loginout',params,'H0001')

// 查询职位列表信息
const getPostList = (params) => request(url+'/web/PositionQuery',params,'L0012')

// 获取个人信息
const getUser = (params) => request(url+'/web/basicUserinfo',params,'L0006')


// 获取面试官列表
const getInterviewers = (params) => request(url+'/web/getinterviewers',params,'L0105')

// 查询职位信息
const postDetails = (params) => request(url+'/web/lookposition',params,'L0014')

//  新增或修改职位信息
const savePost = (params) => request(url+'/web/saveorupdateposition',params,'L0013')

// 查询招聘流程人员信息
const getProcess = (params) => request(url+'/web/queryResume',params,'L0016')

// 查询面试登记信息
const intepinfo = (params) => request(url+'/web/intepinfo',params,'L0100')

// 查询招聘流程人员详细信息
const getResume = (params) => request(url+'/web/getResumeById',params,'L0017')

// 招聘流程阶级招聘更新
const updateProcess = (params) => request(url+'/web/changeStageStatus',params,'L0020')

// 面试登记-查看九型人格测试结果
const getgram = (params) => request(url+'/web/getgram',params,'L0099')

// 查询消息中心
const getnotices = (params) => request(url+'/web/getnotices',params,'L0110')

// 根据消息类型获取消息通知列表
const evaluate = (params) => request(url+'/web/evaluationEdit',params,'L0063')

// 面试官获取面试评估表
const getevaluation = (params) => request(url+'/web/getevaluation',params,'L0108')

// 获取面试评估表内容
const getFeedback = (params) => request(url+'/employeeinfo/getResumeEvaluationById',params,'L0066')

// 简历上传
const resumeUpload = (params) => request(url+'/web/resumeUpload',params,'L0030')

// 查询是否通知面试官
const  queryNotice = (params) => request(url+'/web/getResumeById',params,'L0017')

// 通知Hr安排面试
const  arrangeHr = (params) => request(url+'/web/interec',params,'L0121')


export {
    baseurl,
    arrangeHr,
    queryNotice,
    // qrtq,
    login,
    vcode,
    changepwd,
    wxlogin,
    getphone,
    loginout,
    getPostList,
    getUser,
    getInterviewers,
    postDetails,
    savePost,
    getProcess,
    intepinfo,
    getResume,
    updateProcess,
    getgram,
    getnotices,
    evaluate,
    getevaluation,
    getFeedback,
    resumeUpload,
    getcode,
    notice,
    getInterview,
    remind,
    checkInfo
}
