import Vuex from '@wepy/x';

export default new Vuex.Store({
  state: {
    counter: 0,
    evaluate:null,
    unread:null
  },
  mutations: {
    increment (state) {
      state.counter++;
    },
    decrement (state) {
      state.counter--;
    },
    changeE(state, val) {
      state.evaluate = val;
    },
    // 消息通知（notice-new）
    changeNotice(state,val){
      state.unread=val
    }

  },
  actions: {
    increment ({ commit }) {
      commit('increment');
    },
    decrement ({ commit }) {
      commit('decrement');
    },
    incrementAsync ({ commit }) {
      setTimeout(() => {
        commit('increment');
      }, 1000);
    }
  }
});
